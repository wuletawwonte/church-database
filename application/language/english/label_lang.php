<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['Username'] = 'Username';
$lang['Password'] = 'Password';
$lang['Dashboard'] = 'Dashboard';
$lang['Churches'] = 'Churches';
$lang['cal_th'] = 'Th';
$lang['cal_fr'] = 'Fr';
$lang['cal_sa'] = 'Sa';
